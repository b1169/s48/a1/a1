import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'; 

//from React Context
import { UserProvider } from './UserContext'

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';



export default function App() {

      const [user, setUser] = useState({
            email: localStorage.getItem('email')

      })

      //function for clearing localStorage on logout

      const unsetUser = () => {
        localStorage.clear();
      }

  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar/>
          <Container fluid className="m-3">
              <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/courses" component={Courses} />
                    <Route exact path="/Register" component={Register} />
                    <Route exact path="/Login" component={Login} />
                    <Route exact path="/logout" component={Logout} />
                    <Route component={Error} />
              </Switch>
          </Container>
      </BrowserRouter>
      </UserProvider>
    )
}

