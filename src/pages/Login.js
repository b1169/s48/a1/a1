import React from 'react'

import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button, Form} from 'react-bootstrap';

import UserContext from '../UserContext'

export default function Login(){

	//useContext = hook???
	const { user, setUser} = useContext(UserContext);

	//useState
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect( () => {

		if(email !== "" && password !== ""){
			setIsDisabled(false)
		}

	}, [email,password])
	
	function Login(e){
		e.preventDefault()
		alert("Logged In Successfully")

		/*store email in the local storage*/
		// localStorage.setItem("propertyName", value)
		localStorage.setItem("email", email)

		//state ang pina=pass kay setUser
		setUser({
			email: localStorage.getItem('email')
		})

		setEmail("")
		setPassword("")
	}

	return(

		<Container>
			<Form className="border p-3 mb-3" onSubmit={ (e) => Login(e)}>
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    		type="email" 
			    		placeholder="Enter email" 
			    		value={email}

			    		onChange ={ (e) => setEmail(e.target.value)}
			    		

			    		/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value={password}
				
				    	onChange ={ (e) => setPassword(e.target.value)}	
			    	/>
			  </Form.Group>
			 
			  <Button variant="success" type="submit" disabled={isDisabled}>
			    Login
			  </Button>
			</Form>

		</Container>
		)
}