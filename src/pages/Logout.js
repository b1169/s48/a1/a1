import {Redirect} from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout(){

	const { unsetUser, setUser} = useContext(UserContext);
	
	//Clear the localStorage of the user's info
	unsetUser();

	useEffect(() => {

		setUser({email: null})
	}, [])

	//empty array to avid infinite loop - to render only once


	return(

		<Redirect to="/login"/>
	)
}