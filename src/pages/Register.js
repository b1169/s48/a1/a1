import {Redirect} from 'react-router-dom';
import {Container, Row, Col, Card, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import React from 'react'
import UserContext from '../UserContext'


export default function Register(){

	const { user } = useContext(UserContext);
	//useState
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect( () => {

		if(email !== "" && password !== "" && cpw !== "" && (cpw===password)){
			setIsDisabled(false)
		}

	}, [email,password,cpw] )

	function Register(e){
		e.preventDefault()
		alert("Registered Successfully")

		setEmail("")
		setPassword("")
	}

	if(user.email === null){

	return(

		<Container>
			<Form className="border p-3 mb-3" onSubmit={(e) =>Register}>
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    		type="email" 
			    		placeholder="Enter email" 
			    		value={email}

			    		onChange ={ (e) => setEmail(e.target.value)}
			    		

			    		/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value={password}
				
				    	onChange ={ (e) => setPassword(e.target.value)}	
			    	/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="confirm">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
				    	type="password" 
				    	placeholder="Confirm Password" 
				    	value={cpw}

				    	onChange ={ (e) => setCpw(e.target.value)}
				    
			    	/>
			  </Form.Group>

			 
			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			</Form>

		</Container>
	
		
		)
	}	

	else{
		return(
			<Redirect to="/courses"/>
			)
	}
}