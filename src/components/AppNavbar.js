
import {Navbar, Nav} from 'react-bootstrap';
import {Link, NavLink, useHistory} from 'react-router-dom';
import {useState, useContext} from 'react';
import {Fragment} from 'react';
import UserContext from '../UserContext';

export default function AppNavbar() {
//getItem vs setItem
//const [user, setUser] = useState(localStorage.getItem("email"))

  const { user } = useContext(UserContext);

  let leftNav = (user.email != null) ? 

  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>

  :

  <Fragment>
    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
  </Fragment>


  return(

      <Navbar bg="primary" expand="lg">
        <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
          </Nav>
          <Nav>
            {leftNav}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
}
