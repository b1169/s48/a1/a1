import React from 'react'

//Creates a Context object

//A context object as the name states is a data type of an object that can be used  to store information that can be shared to other components within the app.

const UserContext = React.createContext();

//The 'Provider' component allows other components to consume or to use the context objects and supply the necessary information needed to the context object (states that we pass e.g. user, setUser)

export const UserProvider = UserContext.Provider

export default UserContext;